SET @total := (SELECT COUNT(*) FROM user);
SELECT COUNT(*) / @total * 100 AS Percentage
FROM user
WHERE mvp_score > 1;