import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { SelfBetOverviewComponent } from './views/self-bet-overview/self-bet-overview.component';
import { SelfCreatedTopicsComponent } from './views/self-created-topics/self-created-topics.component';
import { PublicTopicOverviewComponent } from './views/public-bet-overview/public-topic-overview.component';
import { MenuComponent } from "./views/menu/menu.component";
import { LoginComponent } from "./views/login/login.component";
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { SpecificTopicComponent } from './views/specific-topic/specific-topic.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from "@angular/material/icon";
import { CreateTopicComponent } from './views/create-topic/create-topic.component';
import { LeaderboardComponent } from './views/leaderboard/leaderboard.component';
import { AdaptRowsDirective } from './common/adapt-rows/adapt-rows.directive';
import { ResolveComponent } from './views/resolve/resolve.component';
import { HeaderComponent } from './views/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    SelfBetOverviewComponent,
    HomeComponent,
    SelfCreatedTopicsComponent,
    PublicTopicOverviewComponent,
    MenuComponent,
    LoginComponent,
    SpecificTopicComponent,
    CreateTopicComponent,
    LeaderboardComponent,
    AdaptRowsDirective,
    ResolveComponent,
    HeaderComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
