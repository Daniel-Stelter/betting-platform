import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthenticationService} from "./authentication.service";

@Injectable({
  providedIn: 'root'
})
export class RequestSenderService {

  private serverUrl: string = 'http://localhost:8080/'

  constructor(private httpClient: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  public sendRequest(endpoint: string, type: string, data?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      const urlString = this.serverUrl + endpoint;
      switch (type) {
        case 'post': {
          this.httpClient.post(urlString, data, {headers: this.authenticationService.Headers}).subscribe((response: any) => {
            if (!response.error) {
              resolve(response);
            } else {
              reject('error');
            }
          }, error => {
            console.log(error);
            reject('error');
          });
          break;
        }
        case 'get': {
          console.log(this.authenticationService.Headers);
          this.httpClient.get(urlString, {headers: this.authenticationService.Headers}).subscribe((response: any) => {
            if (!response.error) {
              resolve(response);
            } else {
              reject('error');
            }
          }, error => {
            console.log(error);
            reject('error');
          })
          break;
        }
        case 'patch': {
          this.httpClient.patch(urlString, data, {headers: this.authenticationService.Headers}).subscribe((response: any) => {
            if (!response.error) {
              resolve(response);
            } else {
              reject('error');
            }
          }, error => {
            console.log(error);
            reject('error');
          });
          break;
        }
        default: {
          console.log('unknown HTTP request type');
        }
      }
    }).catch(reason => console.log(reason));
  }
}
