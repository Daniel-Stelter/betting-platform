import { Injectable } from '@angular/core';
import {HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private loginToken: string;
  private headers: HttpHeaders = new HttpHeaders();

  constructor() { }

  public getLoginToken(): string {
    return this.loginToken;
  }

  public setLoginToken(token: string) {
    this.loginToken = token;
    this.headers = this.headers.set('Auth', 'Bearer ' + token);
    this.headers = this.headers.set('Access-Control-Allow-Origin', '*');
    this.headers = this.headers.set('Content-Type', 'application/json')
    console.log(this.headers)
  }

  get Headers(): HttpHeaders{
    return this.headers;
  }

  set Headers(value: HttpHeaders) {
    this.headers = value;
  }
}
