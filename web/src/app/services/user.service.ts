import { Injectable } from '@angular/core';
import {User} from "../types/User";
import {RequestSenderService} from "./request-sender.service";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private user: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  constructor(private requestSender: RequestSenderService) { }

  public requestUser(username: string): void {
    this.requestSender.sendRequest(`user/${username}`, 'get').then(response => {
      this.user.next(response as User);
    });
  }

  public get user$(): Observable<User> {
    return this.user.asObservable();
  }

  public get userId(): number {
    return this.user.value.id;
  }

  public get username(): string {
    return this.user.value.username;
  }

  public setUser(user: User): void {
    this.user.next(user);
  }
}
