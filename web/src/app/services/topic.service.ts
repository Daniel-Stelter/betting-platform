import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {Topic} from "../types/Topic";
import {RequestSenderService} from "./request-sender.service";

@Injectable({
  providedIn: 'root'
})
export class TopicService {

  private topics: BehaviorSubject<Topic[]> = new BehaviorSubject<Topic[]>([]);

  constructor(private requestSender: RequestSenderService) {}

  public async getTopics(): Promise<void> {
    // TODO: do actual HTTP Request
    this.requestSender.sendRequest('topic', 'get').then(response => {
      this.topics.next(response)
    });
  }

  public get topics$(): Observable<Topic[]> {
    return this.topics.asObservable();
  }
}
