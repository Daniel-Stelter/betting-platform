import { Injectable } from '@angular/core';
import {UserService} from "./user.service";
import {BehaviorSubject, filter, Observable} from "rxjs";
import {RequestSenderService} from "./request-sender.service";
import {Bet} from "../types/Bet";

@Injectable({
  providedIn: 'root'
})
export class BetService {

  private userBets: BehaviorSubject<Bet[]> = new BehaviorSubject<Bet[]>(null);

  constructor(private userService: UserService,
              private requestSender: RequestSenderService) {
    this.request();
  }

  public request() : void {
    this.requestSender.sendRequest(`bet/user/${this.userService.userId}`, 'get').then(response => {
      this.userBets.next(response);
    });
  }

  public get userBets$(): Observable<Bet[]> {
    return this.userBets.asObservable();
  }
}
