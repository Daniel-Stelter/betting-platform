import { Directive, ElementRef, HostListener, OnInit } from '@angular/core';

@Directive({ selector: '[appAdaptRows]' })
export class AdaptRowsDirective implements OnInit {

  constructor(private elem: ElementRef) {
    elem.nativeElement.rows = 1;
  }

  @HostListener("window:resize") onWindowResize() {
    this.resize();
  }

  @HostListener("input") onInput() {
    this.resize();
  }

  resize() {
    this.elem.nativeElement.style.height = '0';
    this.elem.nativeElement.style.height = (this.elem.nativeElement.scrollHeight + 10) + 'px';
  }

  ngOnInit(): void {
      this.resize();
  }

}
