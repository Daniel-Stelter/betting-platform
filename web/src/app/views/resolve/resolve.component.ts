import {Component, OnInit} from '@angular/core';
import {RequestSenderService} from "../../services/request-sender.service";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
import {Topic} from "../../types/Topic";
import {BetOption} from "../../types/BetOption";
import {ResolveTopicRequest} from "../../messagin/requests/ResolveTopicRequest";

@Component({
  selector: 'app-resolve',
  templateUrl: './resolve.component.html',
  styleUrls: ['./resolve.component.css']
})
export class ResolveComponent implements OnInit {

  public topicOptions: string[] = [];
  public selectedTopic: Topic;

  public previousTab: number;

  public winningOption: BetOption;

  constructor(private requestSender: RequestSenderService,
              private userService: UserService,
              private router: Router) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.selectedTopic = this.router.getCurrentNavigation().extras.state['topic'];
      this.previousTab = this.router.getCurrentNavigation().extras.state['tab'];
    } else {
      this.router.navigate([''])
    }
  }

  ngOnInit(): void {
  }

  public onSelectWinningOption(opt: BetOption): void {
    this.winningOption = (opt === this.winningOption) ? null : opt;
  }

  public onResolveTopic(): void {
    const resolveRequest: ResolveTopicRequest = {
      betOptionId: this.winningOption.id
    }
    this.requestSender.sendRequest('topic', 'patch', resolveRequest).then(() => {
      this.router.navigate(['home'], {
        state: {
          tab: this.previousTab
        }
      })
    });

  }

  public onReturn(): void {
    this.router.navigate(['home'], {
      state: {
        tab: this.previousTab
      }
    })
  }
}
