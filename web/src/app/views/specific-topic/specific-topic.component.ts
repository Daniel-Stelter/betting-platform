import { Component, OnInit } from '@angular/core';
import { Topic } from "../../types/Topic";
import { Router } from "@angular/router";
import { BetService } from 'src/app/services/bet.service';
import { filter, take } from 'rxjs';
import { BetOption } from 'src/app/types/BetOption';
import { Bet } from 'src/app/types/Bet';
import { CreateBetRequest } from '../../messagin/requests/CreateBetRequest';
import { UserService } from 'src/app/services/user.service';
import { RequestSenderService } from 'src/app/services/request-sender.service';
import { User } from 'src/app/types/User';

@Component({
  selector: 'app-specific-topic',
  templateUrl: './specific-topic.component.html',
  styleUrls: ['./specific-topic.component.css']
})
export class SpecificTopicComponent implements OnInit {

  topic: Topic;
  user: User;
  chosenOption: BetOption; // picked option (may be final if user already placed a bet)
  placedBet: Bet; // only set if bet is placed
  inputCredit: number; // used for creating new bet
  errorMessage: string; // used for any kind of errors
  private previousTab: number;

  constructor(private router: Router,
    private betService: BetService,
    private userService: UserService,
    private reqService: RequestSenderService) {
    // navigation stuff
    if (!this.router.getCurrentNavigation().extras || !this.router.getCurrentNavigation().extras.state) {
      this.onReturn()
    } else {
      this.topic = this.router.getCurrentNavigation().extras.state['topic'];
      this.previousTab = this.router.getCurrentNavigation().extras.state['tab'];
    }
    // get user
    this.userService.user$.pipe(filter(user => user !== null), take(1)).subscribe(user => {
      this.user = user;
    });
    // find out if there is already a placed bet with this account on the topic
    this.betService.userBets$.pipe(filter(bets => bets !== null)).subscribe(bets => {
      bets.forEach(bet => {
        if (bet.topic.id == this.topic.id) {
          this.placedBet = bet;
          this.chosenOption = bet.betOption;
        }
      })
    });
  }

  getInfoMessage(): String {
    if (this.topic && this.topic.winOption) {
      return "This topic is already closed!";
    } else if (this.placedBet) {
      return "You already placed a bet on this topic!";
    } else {
      return "If you want to place a bet, click on the option of your choice, insert "
        + "the amount of credits you want to set on the bottom and confirm."
    }
  }

  getOptionClass(opt: BetOption): String {
    if (this.topic && this.topic.winOption) {
      if (this.topic.winOption.id == opt.id) {
        return "won-option";
      } else if (this.chosenOption.id == opt.id) {
        return "lost-chosen-option";
      } else {
        return "";
      }
    } else if (this.chosenOption && this.chosenOption.id == opt.id) {
      return "chosen-option";
    } else {
      return ""
    }
  }

  onSelect(opt: BetOption): void {
    if (!this.placedBet)
      this.chosenOption = (opt === this.chosenOption) ? null : opt;
  }

  onConfirm(): void {
    if (this.placedBet)
      this.errorMessage = "Already placed a bet!";
    else if (!this.chosenOption)
      this.errorMessage = "Choose an option to bet on.";
    else if (!this.inputCredit)
      this.errorMessage = "Choose an amount of credits you want to place.";
    else if (isNaN(this.inputCredit))
      this.errorMessage = "Insert a valid credit value.";
    else if (this.inputCredit < 1)
      this.errorMessage = "Credit must be positive.";
    else if (this.inputCredit > this.user.credit)
      this.errorMessage = "You don't have enough credits."
    else {
      this.errorMessage = null;
      let req: CreateBetRequest = {
        userId: this.user.id,
        betOptionId: this.chosenOption.id,
        credit: this.inputCredit
      };
      this.reqService.sendRequest("bet", "post", req)
        .then(() => {
          this.betService.request()
        })
        .catch(err => console.log(err));
    }
  }

  public onReturn(): void {
    this.router.navigate(['home'], {
      state: {
        tab: this.previousTab
      }
    })
  }

  ngOnInit(): void {
  }

}
