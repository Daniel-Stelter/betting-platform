import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticationService} from "../../services/authentication.service";
import {TopicService} from "../../services/topic.service";
import {UserService} from "../../services/user.service";
import {filter, take} from "rxjs";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public pageNumber: number = 0;
  constructor(
    private topicService: TopicService,
    private router: Router,
    private authService: AuthenticationService,
    private userService: UserService) {
    if (!authService.getLoginToken()) {
      this.router.navigate([''])
    }
    this.userService.user$.pipe(take(1)).subscribe(user => {
      if (user) {
        this.userService.requestUser(user.username);
      }
    });
    if(this.router.getCurrentNavigation().extras.state) {
      console.log(this.router.getCurrentNavigation().extras.state['tab']);
      this.pageNumber = this.router.getCurrentNavigation().extras.state['tab'];
    }
  }


  public changePageNumber(pageName: string):void {
    if (pageName === "public-bet-overview")
    {
      this.pageNumber = 0;
    }
    else if (pageName === "self-created-topics")
    {
      this.pageNumber = 1;
    }
    else if(pageName === "self-bet-overview")
    {
      this.pageNumber = 2;
    }
  }

  ngOnInit(): void {
    this.topicService.getTopics();
  }

}
