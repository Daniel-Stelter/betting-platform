import { Component, OnInit } from '@angular/core';
import { Output, EventEmitter} from "@angular/core";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public array = [{
    key: 'public-bet-overview',
    value: 'All topics'
  }, {
    key: 'self-created-topics',
    value: 'Created Topics'
  }, {
    key: 'self-bet-overview',
    value: 'Placed Bets'
  }]
  @Output() changePageEvent = new EventEmitter<string>();

  constructor() { }

  public changePage(element:string): void {
    this.changePageEvent.emit(element);
  }

  ngOnInit(): void {
  }

}
