import {Component, OnDestroy, OnInit} from '@angular/core';
import {filter, Observable, Subject, take, takeUntil} from "rxjs";
import {Topic} from "../../types/Topic";
import {TopicService} from "../../services/topic.service";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-self-created-bets',
  templateUrl: './self-created-topics.component.html',
  styleUrls: ['./self-created-topics.component.css']
})
export class SelfCreatedTopicsComponent implements OnInit, OnDestroy {

  public topics: Topic[];

  private unsubscribe: Subject<void> = new Subject<void>();

  constructor(private topicService: TopicService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit(): void {
    this.topicService.topics$.pipe(filter(topics => topics !== null), takeUntil(this.unsubscribe)).subscribe(topics => {
      this.topics = topics.filter(topic => topic.creator.id === this.userService.userId);
    })
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  public onCreateTopic(): void {
    this.router.navigate(['create'], {
      state: {
        tab: 1
      }
    })
  }

  public onSelectTopic(selectedTopic: Topic): void {
    this.router.navigate(['topic'], {
      state: {
        topic: selectedTopic,
        tab: 1
      }
    })
  }

  public onResolveTopic(selectedTopic: Topic): void {
    this.router.navigate(['resolve'], {
      state: {
        topic: selectedTopic,
        tab: 1
      }
    })
  }
}
