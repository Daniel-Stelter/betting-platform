import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfCreatedTopicsComponent } from './self-created-topics.component';

describe('SelfCreatedBetsComponent', () => {
  let component: SelfCreatedTopicsComponent;
  let fixture: ComponentFixture<SelfCreatedTopicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelfCreatedTopicsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfCreatedTopicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
