import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthRequest} from "../../messagin/requests/AuthRequest";
import {RequestSenderService} from "../../services/request-sender.service";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../services/authentication.service";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public creationForm: FormGroup;
  public createAccount: boolean = false;
  public showLoginErrorMessage: boolean = false;
  public showServerCreationError: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private requestSender: RequestSenderService,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {
    this.createForms();
  }

  private createForms(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.creationForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  public onLogin(): void {
    const request: AuthRequest = {
      username: this.loginForm.get('username').value,
      password: this.loginForm.get('password').value
    }
    this.requestSender.sendRequest('auth', 'post', request).then(response => {
      if (response && response.token) {
        this.showLoginErrorMessage = false;
        this.authenticationService.setLoginToken(response.token);
        this.userService.requestUser(this.loginForm.get('username').value);
        this.router.navigate(['home']);
      } else {
        this.showLoginErrorMessage = true;
      }
    });
  }

  public onCreateAccount(): void {
    const request: AuthRequest = {
      username: this.creationForm.get('username').value,
      password: this.creationForm.get('password').value
    };
    this.requestSender.sendRequest('signup', 'post', request).then(response => {
      if (response && !response.error) {
        this.showServerCreationError = false;
        this.userService.setUser(response.user);
        this.authenticationService.setLoginToken(response.token);
        this.router.navigate(['home']);
      } else {
        this.showServerCreationError = true;
      }
    });
  }
}
