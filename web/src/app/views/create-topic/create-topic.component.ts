import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BetOption} from "../../types/BetOption";
import {RequestSenderService} from "../../services/request-sender.service";
import {CreateTopicRequest} from "../../messagin/requests/CreateTopicRequest";
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-topic',
  templateUrl: './create-topic.component.html',
  styleUrls: ['./create-topic.component.css']
})
export class CreateTopicComponent implements OnInit {

  public creationForm: FormGroup;
  public showBetOptionCreation: boolean = false;

  public topicOptions: string[] = [];

  private previousTab: number;

  constructor(private formBuilder: FormBuilder,
              private requestSender: RequestSenderService,
              private userService: UserService,
              private router: Router) {
    this.createForm();
    if (this.router.getCurrentNavigation().extras.state) {
      this.previousTab = this.router.getCurrentNavigation().extras.state['tab'];
    }
  }

  ngOnInit(): void {
  }

  private createForm(): void {
    this.creationForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      betOptionTitle: ['']
    });
  }

  public onCreateBetOption(): void {
    this.showBetOptionCreation = true;
  }

  public onAddBetOption(): void {
    const title: string = this.creationForm.get('betOptionTitle').value;
    this.creationForm.get('betOptionTitle').reset();
    this.topicOptions.push(title);
    this.showBetOptionCreation = false;
  }

  public onPublishTopic(): void {
    const topicRequest: CreateTopicRequest = {
      title: this.creationForm.get('title').value,
      description: this.creationForm.get('description').value,
      creator: this.userService.userId,
      options: this.topicOptions
    }
    this.requestSender.sendRequest('topic', 'post', topicRequest).then(response => {
      this.router.navigate(['topic'], {
        state: {
          topic: response,
          tab: this.previousTab
        }
      });
    });
  }

  public onCancelCreation(): void {
    this.router.navigate(['home'], {
      state: {
        tab: this.previousTab
      }
    })
  }
}
