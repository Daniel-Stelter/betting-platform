import { Component, OnInit } from '@angular/core';
import {filter, Observable, take} from 'rxjs';
import { RequestSenderService } from 'src/app/services/request-sender.service';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/types/User';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {

  user: User = null;
  leaderboard: User[] = [];
  ownPos: number = null;
  public user$: Observable<User>;

  howMany: number = 5; // length of the leaderboard

  constructor(private req: RequestSenderService, private userService: UserService) {
  }

  getUser(): void {
    this.user$ = this.userService.user$;
    this.userService.user$.pipe(filter(user => user !== null), take(1)).subscribe(user => {
      this.user = user;
      this.getLeaderboardAndPosition()
    });
  }

  getLeaderboardAndPosition(): void {
    this.req.sendRequest(`user/top-${this.howMany}`, "get").then(response => {
      this.leaderboard = response;
      // try to find oneself in leaderboard
      this.ownPos = this.leaderboard.findIndex(item => item == this.user) + 1;
      // if not in leaderboard, request position from server
      if (this.ownPos == 0){
        this.req.sendRequest(`user/place/${this.user.username}`, "get").then(response => {
          this.ownPos = response;
        });
      }
    });
  }

  isInLeaderboard(): boolean {
    return this.ownPos < this.leaderboard.length;
  }

  ngOnInit(): void {
    this.getUser();
    this.getLeaderboardAndPosition();
  }

}
