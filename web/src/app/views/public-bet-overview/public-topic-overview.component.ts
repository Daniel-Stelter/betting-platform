import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {TopicService} from "../../services/topic.service";
import {Topic} from "../../types/Topic";
import {Bet} from "../../types/Bet";
import {Router} from "@angular/router";

@Component({
  selector: 'app-public-bet-overview',
  templateUrl: './public-topic-overview.component.html',
  styleUrls: ['./public-topic-overview.component.css']
})
export class PublicTopicOverviewComponent implements OnInit {

  public publicTopics$: Observable<Topic[]>;

  constructor(
    private topicService: TopicService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.publicTopics$ = this.topicService.topics$;
  }

  public onSelectTopic(selectedTopic: Topic): void {
    this.router.navigate(['topic'], {
      state: {
        topic: selectedTopic,
        tab: 0
      }
    })
  }

  public onCreateTopic(): void {
    this.router.navigate(['create'], {
      state: {
        tab: 0
      }
    });
  }
}
