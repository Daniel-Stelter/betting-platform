import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicTopicOverviewComponent } from './public-topic-overview.component';

describe('PublicBetOverviewComponent', () => {
  let component: PublicTopicOverviewComponent;
  let fixture: ComponentFixture<PublicTopicOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublicTopicOverviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicTopicOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
