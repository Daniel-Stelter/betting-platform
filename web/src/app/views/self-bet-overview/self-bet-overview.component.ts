import {Component, OnInit} from '@angular/core';
import {BetService} from "../../services/bet.service";
import {Bet} from "../../types/Bet";
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-self-bet-overview',
  templateUrl: './self-bet-overview.component.html',
  styleUrls: ['./self-bet-overview.component.css']
})
export class SelfBetOverviewComponent implements OnInit {

  public userBets$: Observable<Bet[]>;

  constructor(private betService: BetService,
              private router: Router) { }

  ngOnInit(): void {
    this.userBets$ = this.betService.userBets$;
  }

  public onSelectTopic(selectedBet: Bet): void {
    this.router.navigate(['topic'], {
      state: {
        topic: selectedBet.topic,
        tab: 2
      }
    })
  }
}
