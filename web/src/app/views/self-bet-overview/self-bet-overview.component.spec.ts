import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfBetOverviewComponent } from './self-bet-overview.component';

describe('SelfBetOverviewComponent', () => {
  let component: SelfBetOverviewComponent;
  let fixture: ComponentFixture<SelfBetOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelfBetOverviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfBetOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
