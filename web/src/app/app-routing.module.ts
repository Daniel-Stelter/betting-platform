import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './views/home/home.component';
import {LoginComponent} from "./views/login/login.component";
import {SpecificTopicComponent} from "./views/specific-topic/specific-topic.component";
import {CreateTopicComponent} from "./views/create-topic/create-topic.component";
import {ResolveComponent} from "./views/resolve/resolve.component";

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'topic',
    component: SpecificTopicComponent
  },
  {
    path: 'create',
    component: CreateTopicComponent
  },
  {
    path: 'resolve',
    component: ResolveComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
