export interface CreateBetRequest {
  userId: number;
  betOptionId: number;
  credit: number;
}
