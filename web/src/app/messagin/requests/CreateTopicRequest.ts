import {BetOption} from "../../types/BetOption";

export interface CreateTopicRequest {
  title: string;
  description: string;
  creator: number;
  options: string[];
}
