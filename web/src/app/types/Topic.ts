import {BetOption} from "./BetOption";
import {User} from "./User";

export interface Topic {
  id: number;
  title: string;
  description: string;
  options: BetOption[];
  creator: User;
  winOption: BetOption;
}
