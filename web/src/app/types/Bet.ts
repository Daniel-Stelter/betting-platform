import {BetOption} from "./BetOption";
import {Topic} from "./Topic";
import { User } from "./User";

export interface Bet {
  id: number;
  user: User;
  betOption: BetOption;
  topic: Topic;
  credit: number;
}
