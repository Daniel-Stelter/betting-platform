DROP TABLE IF EXISTS bet;
DROP TABLE IF EXISTS bet_option;
DROP TABLE IF EXISTS topic;
DROP TABLE IF EXISTS user;

CREATE TABLE user (
	id 				INT 		NOT NULL PRIMARY KEY AUTO_INCREMENT,
	username 		VARCHAR(20) NOT NULL UNIQUE,
    password 		VARCHAR(20) NOT NULL,
    credit 			INT			NOT NULL,
    mvp_score		INT			NOT NULL,
    last_bet_placed	DATE
);

CREATE TABLE topic (
	id 			INT 			NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title 		VARCHAR(100) 	NOT NULL,
    description VARCHAR(2000),
    u_id 		INT 			NOT NULL REFERENCES user(id)
    # win_bo_id INT 			REFERENCES bet_option(id)
    # last column must be added later because of circular dependency with bet_option
);

CREATE TABLE bet_option (
	id 		INT 		NOT NULL PRIMARY KEY AUTO_INCREMENT,
    t_id 	INT 		NOT NULL REFERENCES topic(id),
    title 	VARCHAR(50) NOT NULL
);

ALTER TABLE topic
ADD COLUMN win_bo_id INT REFERENCES bet_option(id);

CREATE TABLE bet(
	id 		INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	u_id 	INT NOT NULL REFERENCES user(id),
    bo_id 	INT NOT NULL REFERENCES bet_option(id),
    credit 	INT NOT NULL
);

##########################################################

INSERT INTO user VALUES (1,	 'admin',			'admin', 				99999, 	1, '2022-1-7');
INSERT INTO user VALUES (2,	 'Dirk',			'password', 			500, 	2, '2022-1-6');
INSERT INTO user VALUES (3,	 'Olaf',			'Umarmungen', 			5000, 	2, '2022-1-6');
INSERT INTO user VALUES (4,	 'Tilmans Mom',		'Ho-Oh', 				1500, 	2, '2022-1-7');
INSERT INTO user VALUES (5,	 'Angelo Merte',	'Raute', 				5500, 	2, '2022-1-5');
INSERT INTO user VALUES (6,	 'Erzgebirge Aue',	'Macht ausm Schacht', 	8000, 	1, '2022-1-6');
INSERT INTO user VALUES (7,	 'Gerald von Riva',	'Blaviken', 			3300, 	3, '2022-1-6');
INSERT INTO user VALUES (8,	 'Neymar',			'Auaaaa Schiriiiiiii',	2600, 	1, '2022-1-7');
INSERT INTO user VALUES (9,	 'Markus Söder',	'Markus Söder', 		6300, 	2, '2022-1-7');
INSERT INTO user VALUES (10, 'EA Sports',		'Itzinnägäimm', 		2100, 	1, '2022-1-5');
INSERT INTO user VALUES (11, 'Drachenblut',		'Fußrunter', 			6100, 	2, '2022-1-6');
INSERT INTO user VALUES (12, 'Trump',			'Fake News', 			5200, 	1, '2022-1-7');

INSERT INTO topic VALUES(
	1,
    'Will the USC Magdeburg win their upcoming game?',
    'An interesting description of the unlikelihood of loss with Daniel Stelter as goal keeper ;)',
    2, null);
INSERT INTO topic VALUES(
	2,
    'Will we reach the highest possible score in Startup Engineering 2?',
    'Full score does NOT include extra points',
    2, null);
INSERT INTO topic VALUES(
	3,
    'Will the OvGU pass (finally) be controlled before our next lecture?',
    'Look at the concept the Mensa has',
    2, null);
INSERT INTO topic VALUES(
	4,
    'Will we implement all functionalities we thought of for this website into the final MVP? ',
    'Probably not but who knows... ;)',
    2, null);

INSERT INTO bet_option VALUES(1, 1, 'Of course!');
INSERT INTO bet_option VALUES(2, 1, 'Definetly!');
INSERT INTO bet_option VALUES(3, 2, '42');
INSERT INTO bet_option VALUES(4, 2, 'Yellow?');
INSERT INTO bet_option VALUES(5, 3, 'No');
INSERT INTO bet_option VALUES(6, 3, 'Nada');
INSERT INTO bet_option VALUES(7, 3, 'Niente');
INSERT INTO bet_option VALUES(8, 4, 'Yes');
INSERT INTO bet_option VALUES(9, 4, 'No');

INSERT INTO bet VALUES(1,  1,  1, 1000);
INSERT INTO bet VALUES(2,  2,  1, 500);
INSERT INTO bet VALUES(3,  2,  3, 300);
INSERT INTO bet VALUES(4,  3,  2, 500);
INSERT INTO bet VALUES(5,  3,  3, 200);
INSERT INTO bet VALUES(6,  4,  4, 100);
INSERT INTO bet VALUES(7,  4,  6, 500);
INSERT INTO bet VALUES(8,  5,  2, 800);
INSERT INTO bet VALUES(9,  5,  7, 1100);
INSERT INTO bet VALUES(10,  6,  6, 200);
INSERT INTO bet VALUES(11, 7,  3, 100);
INSERT INTO bet VALUES(12, 7,  1, 400);
INSERT INTO bet VALUES(13, 7,  5, 600);
INSERT INTO bet VALUES(14, 8,  8, 1500);
INSERT INTO bet VALUES(15, 9,  9, 1200);
INSERT INTO bet VALUES(16, 9,  2, 400);
INSERT INTO bet VALUES(17, 10, 4, 700);
INSERT INTO bet VALUES(18, 11, 6, 9000);
INSERT INTO bet VALUES(19, 11, 2, 700);
INSERT INTO bet VALUES(20, 12, 5, 1300);