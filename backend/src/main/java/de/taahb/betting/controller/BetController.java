package de.taahb.betting.controller;

import de.taahb.betting.entities.*;
import de.taahb.betting.exceptions.InvalidArgumentsException;
import de.taahb.betting.exceptions.MyException;
import de.taahb.betting.messages.BetRequest;
import de.taahb.betting.repo.BetOptionRepo;
import de.taahb.betting.repo.BetRepo;
import de.taahb.betting.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/bet")
@CrossOrigin(origins="*")
public class BetController {

    @Autowired
    private BetRepo betRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private BetOptionRepo boRepo;

    @GetMapping("")
    public List<BetDTO> all() {
        return betRepo.findAll().stream().map(Bet::getDTO).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<BetDTO> byId(@PathVariable Integer id) {
        BetDTO bet = betRepo.findById(id).orElseThrow(EntityNotFoundException::new).getDTO();
        return ResponseEntity.ok(bet);
    }

    @GetMapping("/user/{uId}")
    public List<BetDTO> byUserId(@PathVariable Integer uId) {
        return betRepo.findByUserId(uId).stream().map(Bet::getDTO).collect(Collectors.toList());
    }

    @GetMapping("/option/{boId}")
    public List<BetDTO> byBetOptionId(@PathVariable Integer boId) {
        return betRepo.findByBetOptionId(boId).stream().map(Bet::getDTO).collect(Collectors.toList());
    }

    @Transactional
    @PostMapping("")
    public ResponseEntity<BetDTO> create(@RequestBody BetRequest req) throws MyException {
        Integer userId = req.getUserId();
        Integer optionId = req.getBetOptionId();
        Integer credit = req.getCredit();
        if (userId == null || optionId == null || credit == null)
            throw new InvalidArgumentsException();

        User user = userRepo.findById(userId).orElseThrow(() -> new MyException("No such user found"));
        BetOption option = boRepo.findById(optionId).orElseThrow(() -> new MyException("No such bet option found"));

        if (option.getTopic().isResolved())
            throw new MyException("Cannot create a bet on an already resolved topic");
        if (credit == 0)
            throw new MyException("Cannot create a bet without any credit set");
        if (credit < 0)
            throw new MyException("Cannot create a bet with a negative credit");
        if (user.getCredit() < credit)
            throw new MyException("Set credit exceeds account balance");
        if (userRepo.numBetsFromSameTopicAsOption(user, option) > 0)
            throw new MyException("Already placed a bet on this topic");

        // update user (credit and MVP stuff)
        user.setCredit(user.getCredit() - credit);
        if (user.getLastBetPlaced() == null || user.getLastBetPlaced().getDay() != new Date().getDay())
            user.setMvpScore(user.getMvpScore() + 1);
        user.setLastBetPlaced(new Date());
        userRepo.save(user);

        Bet bet = new Bet(user, option, credit);
        return ResponseEntity.ok(betRepo.save(bet).getDTO());
    }

}
