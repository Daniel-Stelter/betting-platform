package de.taahb.betting.controller;

import de.taahb.betting.Constants;
import de.taahb.betting.exceptions.InvalidArgumentsException;
import de.taahb.betting.exceptions.MyException;
import de.taahb.betting.entities.User;
import de.taahb.betting.exceptions.Validator;
import de.taahb.betting.repo.UserRepo;
import de.taahb.betting.messages.AuthRequest;
import de.taahb.betting.messages.AuthResponse;
import de.taahb.betting.security.JwtUtil;
import de.taahb.betting.security.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins="*")
public class AuthController {

    @Autowired
    private UserRepo repo;

    @Autowired
    private AuthenticationManager authManager;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/auth")
    public ResponseEntity<AuthResponse> authenticate(@RequestBody AuthRequest req) {
        authManager.authenticate(new UsernamePasswordAuthenticationToken(req.getUsername(), req.getPassword()));
        User user = (User) userDetailsService.loadUserByUsername(req.getUsername());
        String token = jwtUtil.generateToken(user);
        return ResponseEntity.ok(new AuthResponse(token, user));
    }

    @PostMapping("/signup")
    public ResponseEntity<AuthResponse> createUser(@RequestBody AuthRequest req) throws MyException {
        String name = req.getUsername();
        String pass = req.getPassword();
        if (name == null || pass == null)
            throw new InvalidArgumentsException();
        Validator.checkLength(name, Constants.USERNAME_MIN_LEN, Constants.USERNAME_MAX_LEN, "username");
        Validator.checkLength(pass, Constants.PASSWORD_MIN_LEN, Constants.PASSWORD_MAX_LEN, "password");

        User user = new User(name, pass, Constants.USER_START_CREDIT, 0, null);
        try {
            repo.save(user);
        } catch (Exception e) {
            throw new MyException("Username already exists");
        }
        String token = jwtUtil.generateToken(user);
        return ResponseEntity.ok(new AuthResponse(token, user));
    }
}
