package de.taahb.betting.controller;

import de.taahb.betting.entities.UserDTO;
import de.taahb.betting.entities.User;
import de.taahb.betting.exceptions.MyException;
import de.taahb.betting.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins="*")
public class UserController implements UserDetailsService {

    @Autowired
    private UserRepo repo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repo.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("Username not found"));
    }

    @GetMapping("")
    public List<UserDTO> all() {
        return repo.findAll().stream().map(User::getDTO).collect(Collectors.toList());
    }

    @GetMapping("/{username}")
    public ResponseEntity<UserDTO> one(@PathVariable String username) throws MyException {
        User user = repo.findByUsername(username).orElseThrow(() -> new MyException("Username not found"));
        return ResponseEntity.ok(user.getDTO());
    }

    @GetMapping("/top-{num}")
    public List<UserDTO> topTen(@PathVariable int num) {
        return repo.findAll(PageRequest.of(0, num, Sort.by("credit").descending()))
                .stream().map(User::getDTO).collect(Collectors.toList());
    }

    @GetMapping("/place/{username}")
    public ResponseEntity<Integer> placeInLeaderboard(@PathVariable String username) throws MyException {
        User user = repo.findByUsername(username).orElseThrow(() -> new MyException("Username not found"));
        int place = repo.findAll(Sort.by("credit").descending()).indexOf(user) + 1;
        return ResponseEntity.ok(place);
    }
}
