package de.taahb.betting.controller;

import de.taahb.betting.Constants;
import de.taahb.betting.entities.*;
import de.taahb.betting.exceptions.InvalidArgumentsException;
import de.taahb.betting.exceptions.MyException;
import de.taahb.betting.exceptions.Validator;
import de.taahb.betting.messages.TopicRequest;
import de.taahb.betting.repo.BetOptionRepo;
import de.taahb.betting.repo.TopicRepo;
import de.taahb.betting.repo.UserRepo;
import de.taahb.betting.security.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/topic")
@CrossOrigin(origins = "*")
public class TopicController {

    @Autowired
    private TopicRepo topicRepo;

    @Autowired
    private BetOptionRepo boRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private JwtUtil jwtUtil;

    @GetMapping("")
    public List<TopicDTO> all() {
        return topicRepo.findAllUnresolved().stream().map(Topic::getDTO).collect(Collectors.toList());
    }

    @PostMapping("")
    @Transactional
    public ResponseEntity<TopicDTO> create(@RequestBody TopicRequest req) throws MyException {
        String title = req.getTitle();
        String description = req.getDescription();
        Integer creatorId = req.getCreator();
        List<String> rawOptions = req.getOptions();
        // basic validation: needed objects provided?
        if (title == null || creatorId == null || rawOptions == null)
            throw new InvalidArgumentsException();
        // validation of title and description lengths and options size
        Validator.checkLength(title, Constants.TOPIC_TITLE_MIN_LEN, Constants.TOPIC_TITLE_MAX_LEN, "title");
        if (description != null)
            Validator.checkLength(description, null, Constants.TOPIC_DESCRIPTION_MAX_LEN, "description");
        Validator.checkSize(rawOptions, Constants.BET_OPTIONS_MIN_COUNT, Constants.BET_OPTIONS_MAX_COUNT, "options");
        // does the suer exist?
        User creator = userRepo
                .findById(req.getCreator())
                .orElseThrow(() -> new MyException("The provided creator is not known"));
        // create topic (options must be added later)
        Topic topic = new Topic(title, description, creator);
        // validate and create options step by step
        List<BetOption> options = new ArrayList<>();
        for (String raw : rawOptions) {
            Validator.checkLength(raw, Constants.BET_OPTION_MIN_LEN, Constants.BET_OPTION_MAX_LEN, "an option");
            BetOption option = new BetOption(raw, topic);
            options.add(option);
        }
        // add options and save topic
        topic.setOptions(options);
        topicRepo.save(topic);

        return ResponseEntity.ok(topic.getDTO());
    }

    @PatchMapping("")
    @Transactional
    public ResponseEntity<TopicDTO> resolveTopic(HttpServletRequest req, @RequestBody Map<String, Integer> params) throws MyException {
        // find out which topic has to be resolved
        // for this we get the id of an option (the one which has won)
        Integer optionId = params.get("betOptionId");
        if (optionId == null)
            throw new InvalidArgumentsException();
        BetOption option = boRepo.findById(optionId).orElseThrow(() -> new MyException("There is no bet option with the id"));
        Topic topic = option.getTopic();
        // a topic cannot be resolved multiple times
        if (topic.isResolved())
            throw new MyException("The topic was already resolved");
        // now we have to check if the user sending the request is the creator of the bet
        // cut away the "Bearer " prefix of Auth
        // this is fine to do without tests because the TokenFilter already checked this
        String token =  req.getHeader("Auth").substring(7);
        // with this token we can now find the user and compare it to the creator of the topic
        User creator = userRepo.findByUsername(jwtUtil.extractUsername(token)).orElseThrow();
        if (!creator.equals(topic.getCreator()))
            throw new MyException("Not the creator of this bet");
        // the following only needs to be executed if there is at least one winner
        if (!option.getBets().isEmpty()) {
            // update credit of every winning user of the bet
            int totalSum = topic.getCreditSum();
            for (Bet bet : option.getBets()) {
                User user = bet.getUser();
                int win = (int)(totalSum * user.getWinPortionOfBet(bet));
                user.setCredit(user.getCredit() + win);
                userRepo.save(user);
            }
        }
        topic.setWinOption(option);
        topicRepo.save(topic);
        return ResponseEntity.ok(topic.getDTO());
    }
}