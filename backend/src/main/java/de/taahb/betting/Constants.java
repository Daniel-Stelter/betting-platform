package de.taahb.betting;

public class Constants {

    // constants with LEN refer to length of strings
    // constants with COUNT refer to size of lists

    public static final int USERNAME_MIN_LEN = 4;
    public static final int USERNAME_MAX_LEN = 20;

    public static final int PASSWORD_MIN_LEN = 4;
    public static final int PASSWORD_MAX_LEN = 20;

    public static final int TOPIC_TITLE_MIN_LEN = 1;
    public static final int TOPIC_TITLE_MAX_LEN = 100;

    public static final int TOPIC_DESCRIPTION_MAX_LEN = 2000;

    public static final int BET_OPTION_MIN_LEN = 1;
    public static final int BET_OPTION_MAX_LEN = 50;

    public static final int BET_OPTIONS_MIN_COUNT = 2;
    public static final int BET_OPTIONS_MAX_COUNT = 100;

    public static final int USER_START_CREDIT = 5000;

}
