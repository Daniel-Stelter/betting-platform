package de.taahb.betting.repo;

import de.taahb.betting.entities.Bet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BetRepo extends JpaRepository<Bet, Integer> {

    List<Bet> findByUserId(Integer uId);

    List<Bet> findByBetOptionId(Integer boId);

}
