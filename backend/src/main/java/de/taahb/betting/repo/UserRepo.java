package de.taahb.betting.repo;

import de.taahb.betting.entities.BetOption;
import de.taahb.betting.entities.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Integer> {

    Optional<User> findByUsername(String username);

    @Query(
            "SELECT COUNT(b) " +
            "FROM Bet b " +
            "JOIN b.betOption o " +
            "WHERE b.user = :user " +
            "AND o.topic IN ( " +
            "   SELECT t " +
            "   FROM Topic t " +
            "   JOIN t.options o2 " +
            "   WHERE o2 = :betOption" +
            ")"
    )
    int numBetsFromSameTopicAsOption(User user, BetOption betOption);
}
