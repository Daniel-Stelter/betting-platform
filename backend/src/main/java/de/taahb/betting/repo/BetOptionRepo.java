package de.taahb.betting.repo;

import de.taahb.betting.entities.BetOption;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BetOptionRepo extends JpaRepository<BetOption, Integer> {
}
