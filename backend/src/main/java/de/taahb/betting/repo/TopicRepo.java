package de.taahb.betting.repo;

import de.taahb.betting.entities.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TopicRepo extends JpaRepository<Topic, Integer> {

    @Query(
            "SELECT t " +
            "FROM Topic t " +
            "WHERE t.winOption = null "
    )
    List<Topic> findAllUnresolved();
}
