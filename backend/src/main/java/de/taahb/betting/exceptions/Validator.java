package de.taahb.betting.exceptions;

import de.taahb.betting.Constants;

import java.util.List;

public class Validator {

    private Validator() { }

    public static void checkLength(String str, Integer minLen, Integer maxLen, String type) throws MyException {
        if (minLen != null && str.length() < minLen)
            throw new StringTooShortException(type, Constants.TOPIC_TITLE_MIN_LEN, Constants.TOPIC_TITLE_MAX_LEN);
        if (maxLen != null && str.length() > maxLen)
            throw new StringTooLongException(type, Constants.TOPIC_TITLE_MIN_LEN, Constants.TOPIC_TITLE_MAX_LEN);
    }

    public static void checkSize(List<?> list, Integer minCount, Integer maxCount, String type) throws MyException {
        if (minCount != null && list.size() < minCount)
            throw new ListTooShortException(type, Constants.TOPIC_TITLE_MIN_LEN, Constants.TOPIC_TITLE_MAX_LEN);
        if (maxCount != null && list.size() > maxCount)
            throw new ListTooLongException(type, Constants.TOPIC_TITLE_MIN_LEN, Constants.TOPIC_TITLE_MAX_LEN);
    }

}
