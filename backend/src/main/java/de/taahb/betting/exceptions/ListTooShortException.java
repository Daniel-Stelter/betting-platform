package de.taahb.betting.exceptions;

public class ListTooShortException extends MyException {
    public ListTooShortException(String type, int minCount, int maxCount) {
        super(String.format("Too less %s provided (%d-%d)", type, minCount, maxCount));
    }
}
