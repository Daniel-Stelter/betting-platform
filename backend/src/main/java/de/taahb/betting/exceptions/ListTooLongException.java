package de.taahb.betting.exceptions;

public class ListTooLongException extends MyException {
    public ListTooLongException(String type, int minCount, int maxCount) {
        super(String.format("Too many %s provided (%d-%d)", type, minCount, maxCount));
    }
}
