package de.taahb.betting.exceptions;

public class StringTooShortException extends MyException {
    public StringTooShortException(String type, int minLength, int maxLength) {
        super(String.format("Length of %s is too short (%d-%d)", type, minLength, maxLength));
    }
}
