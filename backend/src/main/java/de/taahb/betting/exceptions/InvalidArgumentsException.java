package de.taahb.betting.exceptions;

public class InvalidArgumentsException extends MyException {
    public InvalidArgumentsException() {
        super("Provided arguments are invalid. If you are a developer: Check your type names. If you are a hacker: Fuck off. If you are a user: please ignore this.");
    }
}
