package de.taahb.betting.exceptions;

public class StringTooLongException extends MyException {
    public StringTooLongException(String type, int minLength, int maxLength) {
        super(String.format("Length of %s is too long (%d-%d)", type, minLength, maxLength));
    }
}
