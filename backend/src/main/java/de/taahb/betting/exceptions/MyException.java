package de.taahb.betting.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MyException extends Exception {
    public MyException(String message) {
        super(message);
    }
}
