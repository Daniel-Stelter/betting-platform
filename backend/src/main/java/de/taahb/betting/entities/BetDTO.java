package de.taahb.betting.entities;

public class BetDTO {

    private final Integer id;
    private final UserDTO user;
    private final BetOptionDTO betOption;
    private final TopicDTO topic;
    private final Integer credit;

    public BetDTO(Bet bet) {
        this.id = bet.getId();
        this.user = bet.getUser().getDTO();
        this.betOption = bet.getBetOption().getDTO();
        this.topic = bet.getBetOption().getTopic().getDTO();
        this.credit = bet.getCredit();
    }

    public Integer getId() {
        return id;
    }

    public UserDTO getUser() {
        return user;
    }

    public BetOptionDTO getBetOption() {
        return betOption;
    }

    public TopicDTO getTopic() { return topic; }

    public Integer getCredit() {
        return credit;
    }
}
