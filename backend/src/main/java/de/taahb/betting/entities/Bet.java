package de.taahb.betting.entities;

import javax.persistence.*;

@Entity
public class Bet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "u_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "bo_id")
    private BetOption betOption;

    @Column
    private Integer credit;

    public Bet() {
    }

    public Bet(User user, BetOption betOption, Integer credit) {
        this.user = user;
        this.betOption = betOption;
        this.credit = credit;
    }

    public Integer getId() { return id; }

    public User getUser() {
        return user;
    }

    public BetOption getBetOption() {
        return betOption;
    }

    public Integer getCredit() {
        return credit;
    }

    public BetDTO getDTO() { return new BetDTO(this); }
}
