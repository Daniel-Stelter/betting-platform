package de.taahb.betting.entities;

public class BetOptionDTO {

    private final Integer id;
    private final String title;

    public BetOptionDTO(BetOption option) {
        this.id = option.getId();
        this.title = option.getTitle();
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() { return title; }
}
