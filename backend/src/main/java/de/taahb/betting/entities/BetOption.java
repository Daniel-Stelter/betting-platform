package de.taahb.betting.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class BetOption {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String title;

    @ManyToOne
    @JoinColumn(name = "t_id")
    private Topic topic;

    @OneToMany
    @JoinColumn(name = "bo_id")
    private List<Bet> bets;

    public BetOption() { }

    public BetOption(String title, Topic topic) {
        this.title = title;
        this.topic = topic;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    public BetOptionDTO getDTO() { return new BetOptionDTO(this); }

    public int getCreditSum() {
        int sum = 0;
        for (Bet bet : getBets())
            sum += bet.getCredit();
        return sum;
    }
}