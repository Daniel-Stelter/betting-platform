package de.taahb.betting.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String title;

    @Column
    private String description;

    @ManyToOne
    @JoinColumn(name = "u_id")
    private User creator;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "t_id")
    private List<BetOption> options;

    @OneToOne
    @JoinColumn(name = "win_bo_id")
    private BetOption winOption;

    public Topic() { }

    public Topic(String title, String description, User creator) {
        this.title = title;
        this.description = description;
        this.creator = creator;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) { this.creator = creator; }

    public List<BetOption> getOptions() {
        return options;
    }

    public void setOptions(List<BetOption> options) {
        this.options = options;
    }

    public BetOption getWinOption() {
        return winOption;
    }

    public void setWinOption(BetOption winOption) {
        this.winOption = winOption;
    }

    public TopicDTO getDTO() { return new TopicDTO(this); }

    public int getCreditSum() {
        int sum = 0;
        for (BetOption opt : getOptions())
            sum += opt.getCreditSum();
        return sum;
    }

    public boolean isResolved() { return winOption != null; }
}
