package de.taahb.betting.entities;

import java.util.List;
import java.util.stream.Collectors;

public class TopicDTO {

    private final Integer id;
    private final String title;
    private final String description;
    private final List<BetOptionDTO> options;
    private final UserDTO creator;
    private final BetOptionDTO winOption;

    public TopicDTO(Topic topic) {
        this.id = topic.getId();
        this.title = topic.getTitle();
        this.description = topic.getDescription();
        this.options = topic.getOptions().stream().map(BetOption::getDTO).collect(Collectors.toList());
        this.creator = topic.getCreator().getDTO();
        this.winOption = topic.isResolved() ? topic.getWinOption().getDTO() : null;
    }

    public Integer getId() { return id; }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public List<BetOptionDTO> getOptions() {
        return options;
    }

    public UserDTO getCreator() { return creator; }

    public BetOptionDTO getWinOption() {
        return winOption;
    }
}
