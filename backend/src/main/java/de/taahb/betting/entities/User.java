package de.taahb.betting.entities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private String username;

    @Column
    private String password;

    @Column
    private Integer credit;

    @Column
    private Integer mvpScore;

    @Column
    private Date lastBetPlaced;

    @OneToMany
    @JoinColumn(name = "u_id")
    private List<Topic> topics;

    @OneToMany
    @JoinColumn(name = "u_id")
    private List<Bet> bets;

    public User() { }

    public User(String username, String password, Integer credit, Integer mvpScore, Date lastBetPlaced) {
        this.username = username;
        this.password = password;
        this.credit = credit;
        this.mvpScore = mvpScore;
        this.lastBetPlaced = lastBetPlaced;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getCredit() {
        return credit;
    }

    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    public Integer getMvpScore() {
        return mvpScore;
    }

    public void setMvpScore(Integer mvpScore) {
        this.mvpScore = mvpScore;
    }

    public Date getLastBetPlaced() {
        return lastBetPlaced;
    }

    public void setLastBetPlaced(Date lastBetPlaced) {
        this.lastBetPlaced = lastBetPlaced;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(() -> "read");
    }

    public UserDTO getDTO() {
        return new UserDTO(this);
    }

    public double getWinPortionOfBet(Bet bet) {
        return  bet.getCredit() / (double) bet.getBetOption().getCreditSum();
    }
}
