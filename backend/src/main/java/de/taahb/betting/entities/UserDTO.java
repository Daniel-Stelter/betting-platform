package de.taahb.betting.entities;

public class UserDTO {

    private final Integer id;
    private final String username;
    private final Integer credit;

    public UserDTO(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.credit = user.getCredit();
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public Integer getCredit() {
        return credit;
    }
}
