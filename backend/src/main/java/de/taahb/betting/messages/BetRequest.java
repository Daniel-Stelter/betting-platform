package de.taahb.betting.messages;

public class BetRequest {

    private final Integer userId;
    private final Integer betOptionId;
    private final Integer credit;

    public BetRequest(Integer userId, Integer betOptionId, Integer credit) {
        this.userId = userId;
        this.betOptionId = betOptionId;
        this.credit = credit;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getBetOptionId() {
        return betOptionId;
    }

    public Integer getCredit() {
        return credit;
    }
}
