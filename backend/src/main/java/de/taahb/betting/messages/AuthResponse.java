package de.taahb.betting.messages;

import de.taahb.betting.entities.User;
import de.taahb.betting.entities.UserDTO;

public class AuthResponse {

    private final String token;
    private final UserDTO user;

    public AuthResponse(String token, User user) {
        this.token = token;
        this.user = user.getDTO();
    }

    public String getToken() {
        return token;
    }

    public UserDTO getUser() {
        return user;
    }
}
