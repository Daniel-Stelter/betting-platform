package de.taahb.betting.messages;

import java.util.List;

public class TopicRequest {

    private final String title;
    private final String description;
    private final Integer creator;
    private final List<String> options;

    public TopicRequest(String title, String description, Integer creator, List<String> options) {
        this.title = title;
        this.description = description;
        this.creator = creator;
        this.options = options;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Integer getCreator() {
        return creator;
    }

    public List<String> getOptions() {
        return options;
    }
}
