package de.taahb.betting.messages;

import org.springframework.http.HttpStatus;

import java.util.Date;

public class ErrorMessage {

    private final Date timestamp;
    private final int status;
    private final String error;

    public ErrorMessage(HttpStatus status, String error) {
        this.timestamp = new Date();
        this.status = status.value();
        this.error = error;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public int getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }
}
