# Betting Platform

Ich nutze den Platz hier jetzt einfach mal, um ein kleines Tutorial für Angular zu machen.

## Erste Schritte

### Schritt 1: Projekt pullen und für IDE entscheiden

git pull sollte jeder für sich selbst entscheiden, ob er das über ein Terminal machen will oder mit Visualisierungssoftware.

Als IDE gibt es zwei Möglichkeiten: VS Code oder JetBrains Webstorm. VS Code ist zwar einfach zu verwenden und ziemlich leicht in der Verwendung, bietet aber meist nicht so gutes Feedback für Clean Code, weniger Shortcuts etc. wie Webstorm. Auf Arbeit verwende ich Webstorm und ich werde es mir wahrscheinlich auch für dieses Projekt installieren.

### Schritt 2: node.js und npm installieren

Um Angular und weitere Packages zu installieren, brauchen wir npm bzw. node.js. Ne kurze Google Suchanfrage sollte dabei helfen.

Die Installation von Angular selbst bzw. einige der anderen Command Line Befehle sind unter https://angular.io/cli zu finden.

### Schritt 3: Mit dem Terminal in den gepullten Ordner navigieren und npm install ausführen

Das installiert alle Packages, die zu diesem Zeitpunkt im Projekt importiert werden.

### Schritt 4: ng serve ausführen

Das baut die Webseite und öffnet einen lokalen Server unter localhost:4200.

Ob man dann noch die Defaultseite von Angular sieht oder schon eigenen Kram ist dann abhängig davon wie tryhardig wir sind :D (Keine Garantie dass ich komplett übertreibe. Ihr habt meine BA gelesen)

## Basic Angular Stuff den wir brauchen

### Komponenten

Angular-Komponenten können über die Konsole mit ng generate component KOMPONENTENNAME erstellt werden. Dies generiert eine HTML, CSS und TS Datei. 

Komponenten sind so ein bisschen dafür da, Quellcode übersichtlicher zu gestalten. Sie können genutzt werden, um komplexere Objekte auf der Webseite gekapselt darzustellen, z.B. um ein div mit verschiedenen Metadaten zu einer Wette als ein Objekt in einer überliegenden HTML-Datei verwenden zu können. Jede Komponente ist eine eigene Klasse mit eigenen Variablen und Methoden. An die Klassendefinition ist ein Decorator angefügt, in dem ein selector, eine templateUrl und eine styleUrl definiert sind.

Der selector kann in anderen HTML-Dateien als Tag genutzt werden, um die Komponente visuell zu laden. Die anderen Definitionen im Decorator verweisen den Compiler auf die entsprechenden Dateien.

#### Angular Lifecycle Methoden

Angular besitzt einen eigenen Lifecycle mit verschiedenen States, bei deren Eintreten eigene Methoden definiert werden können. Eine Klasse könnte z.B. folgendermaßen defniert sein:

class Komponente implements OnInit, OnDestroy {...}

In der Klasse müssen dann Funktionen ngOnInit() und ngOnDestroy() definiert sein. Mit ngOnInit kann man zum Beispiel bestimmte Variablen initalisieren oder eine Liste von Wetten laden.

### ngIf und ngFor

In einem HTML-Tag können die Argumente *ngIf und *ngFor verwendet werden, um entscheiden zu können, ob z.B. eine Wette dargestellt werden soll. Eine Liste von Wetten könnte so initialisieren:

<div *ngFor="let bet of bets">
    <BetElement [activeBet]=bet>
</div>

BetElement ist der Selector einer eigenen Komponente, welche Metadaten zu einer einzigen Wette enthält, die wir darstellen wollen. Das entsprechende bet-Objekt wird an diese Komponente gegeben. Dafür braucht man zwar noch inputs und outputs, aber das ist mir grad zu viel xD.

### Services

Services können über die Konsole mit ng generate service ServiceName erstellt werden. Dies generiert eine TS-Datei, welche eine Klassendefinition enthält. Diese Klasse ist ein Service, welcher als Singleton implementiert ist. In der Anwendung sollten wir z.B. einen BetService implementieren, welcher nur für die Verwaltung von Wetten verantwortlich ist und diese zur Verfügung stellt. Dies stellt sicher, dass eine entsprechende Liste von Wetten nur einmal definiert ist.

Services können außerdem relativ einfach in Komponenten genutzt werden. Dafür muss der constructor z.B. so aussehen:

constructor(private betService: BetService){...}

Dies "injected" den Service in die Komponente, sodass er dort wie ein Objekt verwendet werden kann.

### Event und Property Binding

Kann man ganz gut an Beispielen erklären:

<button (click)="addNewBet()">
</button>

Wenn der Button geklickt wird, wird die Methode addNewBet() aufgerufen

<div>
    Titel der Wette: {{bet.title}}
</div>

Angenommen, in der TS-Datei der Komponente ist eine Variable bet definiert. Das Template kommt mit zwei geschweiften Klammern an die Variable ran und inseriert den Inhalt der Eigenschaft title auf die Seite, wenn bet public definiert ist.

## Notes

Man muss nicht jedes Mal neu ng serve aufrufen, wenn man was am Code geändert hat. Das wird automatisch übernommen und mit den Änderungen live neu geladen.

Bei Fragen: Angular Tour of Heroes durcharbeiten. https://angular.io/tutorial

## Stuff den man sich noch anschauen kann wenn man Lust hat

- Bootstrap
- Observables und Subjects, in dem Zuge Callback-Funktionen
- HTTP-Requests und Verarbeitung der Antwort
- Routing


